import time
import zmq
import numpy as np
import pandas as pd
from splineM import spline
import forexLSTMData as price
import tensorflow as tf
import model as model
import input_fn as input_fn

directory = "logs/"
filename = "conv2000seq5"
seq_len = 15
action_space = ["None","Trade"]
batch_size = 200

n_nodes = 128
rnn_layer = 2
n_class = 2
pkeep = 0.9
learning_rate = 0.001


config = tf.estimator.RunConfig(tf_random_seed=1, save_summary_steps=1)
estimator = tf.estimator.Estimator(
    model_fn=model.model,
    model_dir=directory + filename,
    config=config,
    params={
        'rnn_layer': rnn_layer,
        'pkeep': pkeep,
        'n_nodes':n_nodes,
        'n_class':n_class,
        'seq_length':seq_len,
        'learning_rate':learning_rate,
    })


features = ['state','win','loss','r_close','r_high','r_low','r_open']

counter = 0
context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:5558")

def parse_message(message):
	message = str(message).strip("b'")
	lst = message.split("|")
	message_id = lst.pop(0)
	# lst = [float(i) for i in lst]
	data = np.array(lst)
	data = data.reshape(-1,5)
	df = pd.DataFrame(data)
	df = df.convert_objects(convert_numeric=True)
	df.columns = ['date','open', 'high','low','close']
	# df["test"] = df.close + df.low
	return df, message_id

def find_spline(data):
	sp = spline(data)   
	spline_df = sp.splineLive(n=len(data))
	spline_list = np.array(spline_df.index)
	# print(spline_df.head(10))
	return spline_list

print("<SPline Server running>")
while True:
    #  Wait for next request from client
    message = socket.recv()
    counter+=1
    print("Data received : reply[%s]" % counter)
    # print(message)
    data, message_id = parse_message(message)
    # print(len(data))
    # print(data)
    res = find_spline(data)
    reply = ""
    for x in res:
      data = data[:seq_len+1]
      data['line'] = x
      p_data = price.online_preprocess(data,features)
      predictions = estimator.predict(input_fn=lambda:input_fn.predict_input_fn(p_data))
      # predictions = predict_fn()
      # print("raw data ",data)
      # print(p_data)
      class_id = 0
      for pred_dict in predictions:
        template = ('line:{}  "{}" ({:.1f}%)')
        class_id = pred_dict['class_ids'][0]
        probability = pred_dict['probabilities'][class_id]
        print(template.format(x, action_space[class_id], 100 * probability))
      # exit()
      reply = reply + str(x) + "*" + str(class_id) +"|" 
      break
    reply = message_id + "|" + reply
    print(reply)
    socket.send(reply.encode("utf-8"))
