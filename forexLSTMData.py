import numpy as np
import pandas as pd
import multiprocessing as mp
import time
import tensorflow as tf
import random
import math
import matplotlib.pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')

random.seed(1)
np.random.seed(1)

trade_threshold = 0.0001
loss_threshold = 0.0005
profit_threshold = 0.0000
line = "line_1"

index50r = 0

def get_state(line,close,ab='a'):
	if line==0:
		if ab == 'a':
			return 0
		elif ab == 'b':
			return 1
	if line > close:
		return 1
	else:
		return 0

def get_win(line, high, low, high_b, low_b, high_a, low_a, state):
	if state == 1:
		if high - line <= loss_threshold and line - high <= trade_threshold and high_a < line and (line-low > profit_threshold or line-low_a > profit_threshold):
			return 1
		else:
			return 0
	if state == 0:
		if line - low <= loss_threshold and low - line <= trade_threshold and line < low_a and (high-line > profit_threshold or high_a-line > profit_threshold):
			return 1
		else:
			return 0

def get_loss(line, high, low, state):
	if state == 1:
		if high - line > loss_threshold :
			return 1
		else:
			return 0
	if state == 0:
		if line - low > loss_threshold :
			return 1
		else:
			return 0

def get_lab(win, loss):
	if win == 1:
		return 1
	elif loss == 1:
		return 0
	else:
		return 0



def readData(dataDir, features):
	global price_data
	data = pd.read_csv(dataDir)
	price_data = data
	data['state'] = list(map(get_state,data[line],data['close']))
	data['win'] = list(map(get_win,data[line],data['high'],data['low'], data.high.shift(-1), data.low.shift(-1), data.high.shift(1), data.low.shift(1),data['state']))
	data['loss'] = list(map(get_loss,data[line],data['high'],data['low'],data['state']))
	data['label'] = list(map(get_lab,data['win'],data['loss']))

	data['r_close'] = data.close.diff(1) * 1000
	data['r_open'] = data.open.diff(1) * 1000
	data['r_high'] = data.high.diff(1) * 1000
	data['r_low'] = data.low.diff(1) * 1000

	data['dif_high'] = (data[line] - data.high ) * 100
	data['dif_low'] = (data[line] - data.low ) * 100
	data['dif_close'] = (data[line] - data.close ) * 100

	data['c_range'] = (data.high - data.low ) * 1000
	data['c_body'] = (data.close - data.open ) * 1000

	features =  ['close',line] + features + ['label']
	data = data[features]
	return data

def online_preprocess(df, features):
	data = df
	data['state'] = list(map(get_state,data['line'],data['close']))
	data['win'] = list(map(get_win,data['line'],data['high'],data['low'], data.high.shift(1), data.low.shift(1), data.high.shift(-1), data.low.shift(-1),data['state']))
	data['loss'] = list(map(get_loss,data['line'],data['high'],data['low'],data['state']))
	data['label'] = list(map(get_lab,data['win'],data['loss']))

	data['r_close'] = data.close.diff(-1) * 1000
	data['r_open'] = data.open.diff(-1) * 1000
	data['r_high'] = data.high.diff(-1) * 1000
	data['r_low'] = data.low.diff(-1) * 1000
	data = data[features]
	data.dropna(inplace=True)
	data = np.array(data)
	data = data[np.newaxis, :]
	return data


def convert_to_sequence(df,seqlen=10):
    # index50r = df.loc[abs(df['line_1'] - df['close']) < 0.0050].index
    global index50r
    index50r = df.loc[(df['loss'] == 1) | (df['win'] == 1)].index

    x = np.array(df.iloc[:,2:-1])
    y = np.array(df.iloc[:,-1])  # Close as label
    couter = 0
    dataX = []
    dataY = []
    for i in index50r:
        _x = x[i - seqlen : i]
        _y = y[i]  # Next close price

        couter+=1
        if(couter <= 10):
            print("..........................................................................",i)
            print(_x, "->", _y)
        dataX.append(_x)
        dataY.append(_y)

    return dataX, dataY


def splitData(dataX, dataY, batch_size, split=0.7):
	train_size = int(len(dataY) * split)
	nb_batches = (train_size - 1) // (batch_size)
	assert nb_batches > 0, "Not enough data, even for a single batch. Try using a smaller batch_size."
	train_size = int(nb_batches * batch_size)
	print("slice index is: ",train_size)
	trainX, testX = np.array(dataX[0:train_size]), np.array(dataX[train_size:len(dataX)])
	trainY, testY = np.array(dataY[0:train_size]), np.array(dataY[train_size:len(dataY)])
	return trainX, trainY, testX, testY

def plot(labels, predictions, mode='test'):
	data_index = index50r[:len(labels)]
	df = price_data[:data_index[-1]]
	if mode == 'test':
		data_index =  index50r[len(index50r) - len(labels):]
		df = price_data[data_index[0]:]
	predict = []
	for x in predictions:
		predict.append(x['class_ids'][0])

	print("index len: ",len(data_index))
	print('labels len: ',len(labels))
	print('predict len: ',len(predict))
	# print(labels)
	# print(predict)
	# exit()
	# print(data.head())
	data = df
	data['labels'] = 0
	data['predictions'] = 0
	data['labels'][data_index] = labels
	data['predictions'][data_index] = predict

	data['win'] = list(map(get_plot_win,data['labels'],data['predictions'],data['close']))
	data['loss'] = list(map(get_plot_loss,data['labels'],data['predictions'],data['close']))

	data['stop_sell'] = data[line] + 0.0005
	data['stop_buy'] = data[line] - 0.0005


	dataw = data.loc[data['win'] == 1]
	datal = data.loc[data['loss'] == 1]
	print("win count:",len(dataw))
	print("loss count:",len(datal))


	fig = plt.figure(figsize=(14,7)) 
	ax1 = plt.subplot2grid((8,1), (0,0), rowspan=6, colspan=1)
	data[line].plot(linewidth=1, ax=ax1)
	data['stop_sell' ].plot(linewidth=1, color='yellow', alpha=0.4, ax=ax1)
	data['stop_buy' ].plot(linewidth=1, color='yellow', alpha=0.4, ax=ax1)

	data['high' ].plot(linewidth=1, color='black', alpha=0.2, ax=ax1)
	data['low'  ].plot(linewidth=1, color='black', alpha=0.2, ax=ax1)
	data['close'].plot(linewidth=1, color='black', alpha=0.4, ax=ax1)
	ax1.scatter(data.index, data['win'], c='blue', alpha=0.9)
	ax1.scatter(data.index, data['loss'], c='red', alpha=0.9)
	plt.show()

def get_plot_win(label, prediction, close):
	if prediction == 1 and label == 1:
		return close
	else:
		return None

def get_plot_loss(label, prediction, close):
	if prediction == 1 and label == 0:
		return close
	else:
		return None
