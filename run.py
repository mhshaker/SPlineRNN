
import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np
import pandas as pd  
import matplotlib.pyplot as plt
import os
import model as model
import input_fn as input_fn
import forexLSTMData as price
import random
random.seed(1)
tf.set_random_seed(1)
np.random.seed(1)

features = ['state','win','loss','dif_high', 'dif_low', 'dif_close'] #'r_close','r_high','r_low','r_open'
seq_length = 1
batch_size = 100

n_nodes = 128
rnn_layer = 2
n_class = 2
steps = 2000
pkeep = 0.9
learning_rate = 0.01
directory = "logs/"
filename = "plot_test"
filepath = '../Data/EURUSD15_spline_K4T4A4B4C3n5000_m.csv'



def main(argv):
    # args = parser.parse_args(argv[1:])

    df = price.readData(filepath, features)
    print(df.head(10))
    x, y = price.convert_to_sequence(df, seq_length)
    print("convert_to_sequence done!")
    trainX, trainY, testX, testY = price.splitData(x,y,batch_size, 0.7)
    print("traind data len:", len(trainY))
    print(np.unique(trainY))
    print(np.bincount(trainY))
    print("test data len:", len(testY))
    print(np.unique(testY))
    print(np.bincount(testY))
    config = tf.estimator.RunConfig(tf_random_seed=1, save_summary_steps=1)

    estimator = tf.estimator.Estimator(
        model_fn=model.model,
        model_dir=directory + filename,
        config=config,
        params={
            'rnn_layer': rnn_layer,
            'pkeep': pkeep,
            'n_nodes':n_nodes,
            'n_class':n_class,
            'seq_length':seq_length,
            'learning_rate':learning_rate,
        })

    train_spec = tf.estimator.TrainSpec(input_fn=lambda:input_fn.read_dataset(trainX, trainY, batch_size), max_steps= steps)

    eval_spec = tf.estimator.EvalSpec(input_fn=lambda:input_fn.read_dataset(testX, testY, batch_size,tf.estimator.ModeKeys.EVAL), steps= steps)

    tf.estimator.train_and_evaluate(estimator, train_spec, eval_spec)
    
    predictions = estimator.predict(input_fn=lambda:input_fn.predict_input_fn(testX),predict_keys=['class_ids'])

    price.plot(testY, predictions)

    # eval_spec = tf.estimator.predict(input_fn=lambda:input_fn.read_dataset(testX, None, batch_size,tf.estimator.ModeKeys.EVAL), steps= steps)
   	# tf.estimator.predict()
    # print('\nTest set accuracy: {accuracy:0.3f}\n'.format(**eval_result))

if __name__ == '__main__':
    tf.logging.set_verbosity(tf.logging.INFO)
    tf.app.run(main)
