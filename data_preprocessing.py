import pandas as pd
import numpy as np
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib import style
style.use('fivethirtyeight')

data = pd.read_csv('../Data/EURUSD15_spline_K4T4A4B4C3n5000_m.csv')
# print(data.tail())

trade_threshold = 0.0001
loss_threshold = 0.0005
profit_threshold = 0.0000
line = "line_1"

def get_state(line,close,ab='a'):
	if line==0:
		if ab == 'a':
			return 0
		elif ab == 'b':
			return 1
	if line > close:
		return 1
	else:
		return 0

def get_win(line, high, low, high_b, low_b, high_a, low_a, state):
	if state == 1:
		if high - line <= loss_threshold and line - high <= trade_threshold and high_a < line and (line-low > profit_threshold or line-low_a > profit_threshold):
			return 1
		else:
			return 0
	if state == 0:
		if line - low <= loss_threshold and low - line <= trade_threshold and line < low_a and (high-line > profit_threshold or high_a-line > profit_threshold):
			return 1
		else:
			return 0

def get_loss(line, high, low, state):
	if state == 1:
		if high - line > loss_threshold :
			return 1
		else:
			return 0
	if state == 0:
		if line - low > loss_threshold :
			return 1
		else:
			return 0

def get_lab(win, loss):
	if win == 1:
		return 1
	elif loss == 1:
		return -1
	else:
		return 0

def get_line(line, position,l1,l2,l3,l4,l5,l6,l7,l8):
	a = [line,l1,l2,l3,l4,l5,l6,l7,l8]
	a.sort()
	line_index = a.index(line)
	# print(a)
	# print(line)
	# print(position)
	if 0 <= line_index + position <= 8:
		# print('res: ',a[line_index + position])
		return a[line_index + position] 
	else:
		return 0

data['state'] = list(map(get_state,data[line],data['close']))
data['win'] = list(map(get_win,data[line],data['high'],data['low'], data.high.shift(1), data.low.shift(1), data.high.shift(-1), data.low.shift(-1),data['state']))
data['loss'] = list(map(get_loss,data[line],data['high'],data['low'],data['state']))
data['label'] = list(map(get_lab,data['win'],data['loss']))


dataw = data.loc[data['win'] == 1]
datal = data.loc[data['loss'] == 1]
data = data.loc[abs(data[line] - data['close']) < 0.0050]

print("win",len(dataw))
print("loss",len(datal))
print(len(data))










