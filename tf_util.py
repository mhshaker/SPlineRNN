
import tensorflow as tf
import random
random.seed(1)
tf.set_random_seed(1)


def make_seq(sess, x, seq_len, n_features):
    N = int(sess.run(tf.size(x)) / n_features)
    print("tf.size",N)
    windows = [tf.slice(x, [b,0], [seq_len,n_features]) for b in range(0, N-seq_len)]
    windows = tf.stack(windows)
    return windows

def denseLayer(input, nodes, pkeep, name):
    with tf.variable_scope(name):
        l = tf.layers.dense(
                input, 
                nodes, 
                activation=None,
                # use_bias=False, 
                bias_initializer=tf.constant_initializer(0.1),
                kernel_initializer=tf.contrib.layers.xavier_initializer(), #tf.random_normal_initializer(mean=0, stddev=0.3),
                name=name
            )
        
        # l = tf.layers.batch_normalization(l)
        l = tf.nn.relu(l)
        l = tf.nn.dropout(l, pkeep, seed=operation_level_seed)
        return l

def convLayer(input, filters, kernel, name):
    with tf.variable_scope(name):
        cl = tf.layers.conv1d(input, filters=filters,  kernel_size=kernel, padding='SAME', activation=None, name=name)                
        # cl = tf.layers.batch_normalization(cl)
        cl = tf.nn.relu(cl)
        # cl = tf.nn.dropout(cl, pkeep, name="dropout_"+name)
        return cl

def squeeze_layer(input, sq_nodes, fire_size, name):
    with tf.variable_scope(name):
        s = convLayer(input, sq_nodes, 1, "squeeze")
        l1 = convLayer(s, fire_size, 3, "conv3")
        l2 = convLayer(s, fire_size, 1, "conv1")
        l = tf.concat([l1, l2], 2)
        return l

def attention(inputs, seq_length):
    with tf.variable_scope('attention'):
        # print("inputs.shape ",inputs.shape)
        hidden_size = inputs.shape[2].value
        # print(inputs.shape, " hidden_size:", hidden_size)
        l = tf.reshape(inputs, [-1, hidden_size])
        l = tf.layers.dense(l, hidden_size, activation=tf.nn.relu)
        logits = tf.layers.dense(l,1, activation=None)
        logits = tf.reshape(logits, [-1, seq_length,1])
        alphas = tf.nn.softmax(logits)
        # print("alphas.shape ",alphas.shape)
        # print("inputs.shape ",inputs.shape)
        encoding = tf.reduce_sum(inputs * alphas, 1)
        return encoding, alphas

def make_hparam_string(learning_rate, dropout):
    return "_lr%.0E_pkeep%s" % (learning_rate, dropout)

def mapIndexToAction(index):
    with tf.variable_scope("mapIndexToAction"):
        def helper(): return tf.cond(tf.equal(index, tf.constant(1,dtype=tf.int64)),isSell,isNon)
        def isBuy():  return 1.0
        def isSell(): return -1.0
        def isNon():  return 0.0
        return tf.cond(tf.equal(index, tf.constant(0,dtype=tf.int64)),isBuy,helper)

def get_spread(action, last_action, spread):
    def _spread(action, last_action):
        s = 0
        if (last_action == 1 and action == -1) or (last_action == -1 and action == 1) or (last_action == 0 and action != last_action):
            s = spread
        return s
    return list(map(_spread, action, last_action))

def make_positive_reward(reward):
    with tf.variable_scope("make_positive_reward"):
        return tf.cond(tf.greater(reward, tf.constant(0,dtype=tf.float32)),lambda: reward,lambda: 1.0)

def make_negative_reward(reward):
    with tf.variable_scope("make_negative_reward"):
        return tf.cond(tf.less(reward, tf.constant(0,dtype=tf.float32)),lambda: tf.abs(reward),lambda: 1.0)


def reward(action, price_, spread):
    with tf.variable_scope("reward"):
        action = tf.map_fn(mapIndexToAction, action, dtype=tf.float32, name="action_map")
        reward = ((price_ * action) - spread) * 10000
        p_reward = tf.map_fn(make_positive_reward, reward, dtype=tf.float32, name="p_reward")
        n_reward = tf.map_fn(make_negative_reward, reward, dtype=tf.float32, name="n_reward")

        return reward, p_reward, n_reward



