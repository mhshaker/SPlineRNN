
import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np
import pandas as pd  
import matplotlib.pyplot as plt
import os
import forexLSTMData as price
import tf_util as util
import random
random.seed(1)
tf.set_random_seed(1)
np.random.seed(1)

def model(features, labels, mode, params):
    # tf.reset_default_graph()
    X = features['input_X']
    log_inputs_hook = tf.train.LoggingTensorHook({"inputs" : X, "labels":labels}, every_n_iter=1)
    l = util.convLayer(X,5,1,"conv1")
    l = util.convLayer(l,5,1,"conv2")
    l = util.convLayer(l,5,1,"conv3")
    l = util.convLayer(l,5,1,"conv4")

    # l = util.convLayer(X, 32, 6, "conv1")
    # l = util.squeeze_layer(l,16,32,"squeeze1")
    # l = tf.layers.max_pooling1d(l,2,2,name="maxpool1")
    # l = util.squeeze_layer(l,16,64,"squeeze2")
    # l = util.squeeze_layer(l,32,64,"squeeze3")
    # l = tf.layers.max_pooling1d(l,2,2,name="maxpool2")
    # l = util.squeeze_layer(l,32,64,"squeeze4")
    # l = util.squeeze_layer(l,16,32,"squeeze5")
    # l = tf.layers.max_pooling1d(l,2,2,name="maxpool3")


    # l = tf.concat(c1,c3, 1)
    # with tf.variable_scope('bi_multiRNN'):
    #     fw_cells = []
    #     bw_cells = []
    #     for i in range(0, params['rnn_layer']):
    #         cell_f = rnn.GRUCell(params['n_nodes'], activation=tf.nn.relu)
    #         cell_b = rnn.GRUCell(params['n_nodes'], activation=tf.nn.relu)
    #         cell_f = rnn.DropoutWrapper(cell_f, output_keep_prob=params['pkeep'])
    #         cell_b = rnn.DropoutWrapper(cell_b, output_keep_prob=params['pkeep'])
    #         fw_cells.append(cell_f)
    #         bw_cells.append(cell_b)

    #     (outputs, output_state_fw, output_state_bw) = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(
    #         cells_bw=bw_cells,
    #         cells_fw=fw_cells,
    #         inputs=l,
    #         dtype=tf.float32)

    #     encoder_final_state = tf.concat((output_state_fw[-1], output_state_bw[-1]), 1)
    
    encoding, alphas = util.attention(l, params['seq_length'])
    logits = tf.layers.dense(encoding, params['n_class'], activation=None, name="logits")
    # im = tf.reshape(logits, [100,2,1,-1])
    # tf.summary.image("image",im)
    # Compute predictions.
    predicted_classes = tf.argmax(logits, 1)
    if mode == tf.estimator.ModeKeys.PREDICT:
        predictions = {
            'class_ids': predicted_classes[:, tf.newaxis],
            'probabilities': tf.nn.softmax(logits),
            'logits': logits,
        }
        return tf.estimator.EstimatorSpec(mode, predictions=predictions)

    # Compute loss.
    loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

    # Compute evaluation metrics.
    accuracy = tf.metrics.accuracy(labels=labels,
                                   predictions=predicted_classes,
                                   name='acc_op')
    metrics = {'accuracy': accuracy}
    tf.summary.scalar('accuracy', accuracy[1])

    if mode == tf.estimator.ModeKeys.EVAL:
        return tf.estimator.EstimatorSpec(mode, loss=loss , eval_metric_ops=metrics)#, evaluation_hooks= [log_inputs_hook])

    # Create training op.
    assert mode == tf.estimator.ModeKeys.TRAIN

    optimizer = tf.train.AdamOptimizer(learning_rate=params['learning_rate'])
    train_op = optimizer.minimize(loss, global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode, loss=loss, train_op=train_op)#, training_hooks = [log_inputs_hook]) # 

