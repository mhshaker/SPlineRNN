#-----------------------------------------------------------------------------------#

import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import time
import random
from random import randint
# from talib.abstract import *
from datetime import datetime
from matplotlib import style

style.use('fivethirtyeight')


class spline:
	"""docstring for spline"""
	def __init__(self, marketOHLCdata):
		super(spline, self).__init__()
		self.df    = marketOHLCdata
		# self.df['Date'] = pd.to_datetime(self.df['Date'], format='%Y-%m-%d %H:%M:%S') # I commneted out this line for server! for other uses turn it on!
		# self.df.set_index('Date',inplace=True)
		self.lines = pd.DataFrame(columns=['Tscore','Kscore','Ascore','Bscore','Cscore_peak','Cscore_trough','totalScore','price'])
		self.lines.set_index('price',inplace=True)

	def make_ichi(self):
		self.df['Tenkan_sen'] = ( self.df['high'].rolling(window=9, min_periods=0).max() + self.df['low'].rolling(window=9, min_periods=0).min() ) / 2
		self.df['Kijun_sen']  = ( self.df['high'].rolling(window=26, min_periods=0).max() + self.df['low'].rolling(window=26, min_periods=0).min() ) / 2
		self.df['Span_A']     = ( self.df['Tenkan_sen'] + self.df['Kijun_sen'] ) / 2
		self.df['Span_B']     = ( self.df['high'].rolling(window=52, min_periods=0).max() + self.df['low'].rolling(window=52, min_periods=0).min() ) / 2
		self.df['Span_A']     =   self.df['Span_A'].shift(26)
		self.df['Span_B']     =   self.df['Span_B'].shift(26)
		self.df['Chikou_Span']=   self.df['close'].shift(-26)

	def make_flat(self):
		self.df['K_flat']     = list(map(is_flat, self.df.Kijun_sen, self.df.Kijun_sen.diff(1)))
		self.df['T_flat']     = list(map(is_flat, self.df.Tenkan_sen, self.df.Tenkan_sen.diff(1)))
		self.df['A_flat']     = list(map(is_flat, self.df.Span_A, self.df.Span_A.diff(1)))
		self.df['B_flat']     = list(map(is_flat, self.df.Span_B, self.df.Span_B.diff(1)))
		self.df['C_peak']     = self.df.Chikou_Span.rolling(window=3,center=True).apply(func=is_peak)
		self.df['C_trough']   = self.df.Chikou_Span.rolling(window=3,center=True).apply(func=is_trough)

	def preprocess(self):
		self.make_ichi()
		self.df = self.df.round(5)
		self.make_flat()
		self.df = self.df.replace('0', np.nan)

		self.df['line_1'] = 0
		self.df['line_2'] = 0
		self.df['line_3'] = 0
		self.df['line_4'] = 0
		self.df['line_5'] = 0
		self.df['line_6'] = 0
		self.df['line_7'] = 0
		self.df['line_8'] = 0
		self.df['line_9'] = 0
		self.df['line_10'] = 0

	def splineLive(self, Kpoint=4, Tpoint=4, Apoint=4, Bpoint=4, Cpoint=3, n=50):
		self.preprocess()
		live_spline = pd.DataFrame(columns=['Tscore','Kscore','Ascore','Bscore','Cscore_peak','Cscore_trough','totalScore','price'])
		live_spline.set_index('price',inplace=True)

		for x in range(0,n): 
			if self.df.K_flat[x] > 0:
				try:
					live_spline.loc[self.df.K_flat[x]].Kscore += 1
				except:
					line = {'Tscore':0, 'Kscore':1, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					live_spline.loc[self.df.K_flat[x]] = line
			if self.df.T_flat[x] > 0:
				try:
					live_spline.loc[self.df.T_flat[x]].Tscore += 1
				except:
					line = {'Tscore':1, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					live_spline.loc[self.df.T_flat[x]] = line
			if self.df.A_flat[x] > 0:
				try:
					live_spline.loc[self.df.A_flat[x]].Ascore += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':1, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					live_spline.loc[self.df.A_flat[x]] = line
			if self.df.B_flat[x] > 0:
				try:
					live_spline.loc[self.df.B_flat[x]].Bscore += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':1, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					live_spline.loc[self.df.B_flat[x]] = line
			if self.df.C_peak[x] > 0:
				try:
					live_spline.loc[self.df.C_peak[x]].Cscore_peak += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':1,'Cscore_trough':0, 'totalScore':0 }
					# live_spline.to_csv('linesDebug.csv')
					live_spline.loc[self.df.C_peak[x]] = line
			if self.df.C_trough[x] > 0:
				try:
					live_spline.loc[self.df.C_trough[x]].Cscore_trough += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':1, 'totalScore':0 }
					live_spline.loc[self.df.C_trough[x]] = line


		live_spline['totalScore'] = (live_spline.Tscore*Tpoint + 
								    live_spline.Kscore*Kpoint + 
								    live_spline.Ascore*Apoint + 
								    live_spline.Bscore*Bpoint + 
								    live_spline.Cscore_peak*Cpoint + 
								    live_spline.Cscore_trough*Cpoint)

		live_spline.sort_values(by=['totalScore','Kscore','Tscore','Ascore','Bscore'], ascending=False, inplace=True)
		
		# print(live_spline.head())
		return live_spline.iloc[0:10]

	def run(self, Kpoint=4, Tpoint=4, Apoint=4, Bpoint=4, Cpoint=3, n=50, save=True):
		self.preprocess()
		lastTime = time.time()
		for x in range(0,len(self.df)): # lef(self.df)
			if self.df.K_flat[x] > 0:
				try:
					self.lines.loc[self.df.K_flat[x]].Kscore += 1
				except:
					line = {'Tscore':0, 'Kscore':1, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					self.lines.loc[self.df.K_flat[x]] = line
			if self.df.T_flat[x] > 0:
				try:
					self.lines.loc[self.df.T_flat[x]].Tscore += 1
				except:
					line = {'Tscore':1, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					self.lines.loc[self.df.T_flat[x]] = line
			if self.df.A_flat[x] > 0:
				try:
					self.lines.loc[self.df.A_flat[x]].Ascore += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':1, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					self.lines.loc[self.df.A_flat[x]] = line
			if self.df.B_flat[x] > 0:
				try:
					self.lines.loc[self.df.B_flat[x]].Bscore += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':1, 'Cscore_peak':0,'Cscore_trough':0, 'totalScore':0 }
					self.lines.loc[self.df.B_flat[x]] = line
			if self.df.C_peak[x] > 0:
				try:
					self.lines.loc[self.df.C_peak[x]].Cscore_peak += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':1,'Cscore_trough':0, 'totalScore':0 }
					# self.lines.to_csv('linesDebug.csv')
					self.lines.loc[self.df.C_peak[x]] = line
			if self.df.C_trough[x] > 0:
				try:
					self.lines.loc[self.df.C_trough[x]].Cscore_trough += 1
				except:
					line = {'Tscore':0, 'Kscore':0, 'Ascore':0, 'Bscore':0, 'Cscore_peak':0,'Cscore_trough':1, 'totalScore':0 }
					self.lines.loc[self.df.C_trough[x]] = line

			if x > n:
				if self.df.K_flat[x-n] > 0:
					self.lines.loc[self.df.K_flat[x-n]].Kscore -= 1
				if self.df.T_flat[x-n] > 0:
					self.lines.loc[self.df.T_flat[x-n]].Tscore -= 1
				if self.df.A_flat[x-n] > 0:
					self.lines.loc[self.df.A_flat[x-n]].Ascore -= 1
				if self.df.B_flat[x-n] > 0:
					self.lines.loc[self.df.B_flat[x-n]].Bscore -= 1
				if self.df.C_peak[x-n] > 0:
					self.lines.loc[self.df.C_peak[x-n]].Cscore_peak -= 1
				if self.df.C_trough[x-n] > 0:
					self.lines.loc[self.df.C_trough[x-n]].Cscore_trough -= 1

				self.lines['totalScore'] = (self.lines.Tscore*Tpoint + 
										    self.lines.Kscore*Kpoint + 
										    self.lines.Ascore*Apoint + 
										    self.lines.Bscore*Bpoint + 
										    self.lines.Cscore_peak*Cpoint + 
										    self.lines.Cscore_trough*Cpoint)

				self.lines.sort_values(by=['totalScore','Kscore','Tscore','Ascore','Bscore'], ascending=False, inplace=True)
				#self.lines.to_csv('Data/lines/lines_'+str(x)+'.csv')
				
				for y in range(16,26):
					# print(self.lines.index[y-16])
					# print(self.lines)
					self.df.iloc[x,y] = self.lines.index[y-16]
					# print(np.where(self.lines.totalScore == self.lines.iloc[y-16].totalScore)[0][0] )
			if x % 1000 == 0:
				stepTime = time.time() - lastTime
				print('index ', x, ' out of ', len(self.df), ' is done in ', stepTime, ' seconds.')
				lastTime = time.time()
				# print(df.iloc[x-10:x])
		if save:
			fileName = 'Data'+'\spline_K'+str(Kpoint)+'T'+str(Tpoint)+'A'+str(Apoint)+'B'+str(Bpoint)+'C'+str(Cpoint)+'n'+str(n)+'_n.csv'
			self.df.to_csv(fileName)
		return df

	def run_random(self, n=5000, save=True):
		self.preprocess()

		rand = []
		switch = 0
		counter = 10000
		lastTime = time.time()
		for x in range(n,len(self.df)): 		

			if counter > switch:
				maxPrice = self.df[x-n:x].close.max()
				minPrice = self.df[x-n:x].close.min()
				print(maxPrice)
				rand = [random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice),
						random.uniform(minPrice, maxPrice)]
				switch = randint(1,900)
				counter = 0
			
			for y in range(16,26):
				self.df.iloc[x,y] = rand[y-16]
			counter += 1
			print("task ", mpIndex," done in",round(time.time()-lastTime,2))
			

		self.df.to_csv('spRandom_n'+str(n)+'.csv')

	def extractSignal(self, shift=0): 
		if shift > 0 : 
			self.df['line_1'] = self.df['line_1'].shift(shift)
			self.df['line_2'] = self.df['line_1'].shift(shift)

		self.df['longSignal10']  = list(map(longSignal, self.df.close, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5, self.df.line_6, self.df.line_7, self.df.line_8, self.df.line_9, self.df.line_10))
		self.df['shortSignal10'] = list(map(shortSignal, self.df.close, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5, self.df.line_6, self.df.line_7, self.df.line_8, self.df.line_9, self.df.line_10))
		self.df['longSignal5']   = list(map(longSignal, self.df.close, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5))
		self.df['shortSignal5']  = list(map(shortSignal, self.df.close, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5))
		self.df['longSignal2']   = list(map(longSignal, self.df.close, self.df.line_1, self.df.line_2))
		self.df['shortSignal2']  = list(map(shortSignal, self.df.close, self.df.line_1, self.df.line_2))

		# self.df['longSignal1']   = list(map(longSignal, self.df.close, self.df.line_1))
		# self.df['shortSignal1']  = list(map(shortSignal, self.df.close, self.df.line_1))
		self.df['tradeSignal']  = list(map(enterTrade, self.df.close, self.df.longSignal2, self.df.shortSignal2))
		self.df['cloudU']    	= list(map(cloudU, self.df.line_1, self.df.line_2))
		self.df['cloudD']    	= list(map(cloudD, self.df.line_1, self.df.line_2))
		self.df['cloudU5']    	= list(map(cloudU, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5))
		self.df['cloudD5']    	= list(map(cloudD, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5))
		self.df['cloudU10']    	= list(map(cloudD, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5, self.df.line_6, self.df.line_7, self.df.line_8, self.df.line_9, self.df.line_10))
		self.df['cloudD10']    	= list(map(cloudU, self.df.line_1, self.df.line_2, self.df.line_3, self.df.line_4, self.df.line_5, self.df.line_6, self.df.line_7, self.df.line_8, self.df.line_9, self.df.line_10))
		# self.df['trend2']		= list(map(trend, self.df.close, self.df.longSignal2, self.df.shortSignal2))
		# self.df['stemp']         = (self.df.trend2.diff(1) != 0).astype('int').cumsum()
		# self.df['sma1']       	= self.df['close'].rolling(window=20, min_periods=0).mean()
		# self.df['trend']         = list(map(trend, self.df.close, self.df.slongSignal))
		# self.df['ATR'] = ATR(self.df)
		# self.df['SMA'] = SMA(self.df, timeperiod=20)
		# self.df['WMA'] = WMA(self.df, timeperiod=20)
		# self.df['hammer'] = CDLHAMMER(self.df)
		# self.df['close_future'] = self.df['close'].shift(-4)
		# self.df['Label'] = list(map(create_labels, self.df['close'], self.df['close_future']))

		self.df = self.df.replace('0', np.nan)

	def performanceTest(self, save=True):

		self.df['stemp'] = (self.df.tradeSignal.diff(1) != 0).astype('int').cumsum()

		signals = pd.DataFrame({'signal'   : self.df.groupby('stemp').tradeSignal.first(), 
								'startDate': self.df.groupby('stemp').Date.first(),
								'endDate'  : self.df.groupby('stemp').Date.last(),
								'openPrice': self.df.groupby('stemp').close.first(),
								'length'   : self.df.groupby('stemp').size(), 
	             	        	'max_up'   : abs(self.df.groupby('stemp').close.first() -  self.df.groupby('stemp').high.max()),
	             	        	'max_down' : abs(self.df.groupby('stemp').close.first() -  self.df.groupby('stemp').low.min()) }).reset_index(drop=True)
		signals.dropna(inplace=True)

		signals['close'] = list(map(self.endDateFix, signals['endDate']))
		signals['max_up'] = list(map(maxFinder, signals['close'], signals['max_up'], signals['signal'], signals['openPrice']))
		signals['max_down'] = list(map(minFinder, signals['close'], signals['max_down'], signals['signal'], signals['openPrice']))
		signals['max_profit'] = list(map(maxProfit, signals['max_up'], signals['max_down'], signals['signal']))
		signals['max_loss'] = list(map(maxLoss, signals['max_up'], signals['max_down'], signals['signal']))

		self.performance = sum((signals.max_profit - signals.max_loss) *10000 / ((signals.length) / 2))
		self.totalProfit = sum((signals.max_profit - signals.max_loss)) * 10000
		self.sumLoss     = sum(signals.max_loss) * 10000
		self.sumProfit   = sum(signals.max_profit) * 10000
		self.trades      = len(signals)

		if save:
			signals.sort_values('max_profit', ascending=False, inplace=True)
			# print(signals.head(),len(signals))
			fileName = 'signals_P' + str(self.performance) + '.csv'
			signals.to_csv(fileName)

		return self.performance

	def plot(self):
		# self.df.set_index('Date',inplace=True)

		fig = plt.figure(figsize=(14,7)) 
		ax1 = plt.subplot2grid((8,1), (0,0), rowspan=6, colspan=1)
		ax2 = plt.subplot2grid((8,1), (6,0), rowspan=2, colspan=1, sharex=ax1)

		# plt.fill_between(self.df.index, self.df['Span_A'], self.df['Span_B'], color='r', alpha=0.2)
		ax1.fill_between(self.df.index, self.df['cloudU'], self.df['cloudD'], color='gray', alpha=0.5)
		# ax1.fill_between(self.df.index, self.df['cloudU5'], self.df['cloudD5'], color='blue', alpha=0.05)

		self.df['high' ].plot(linewidth=1, color='black', alpha=0.2, ax=ax1)
		self.df['low'  ].plot(linewidth=1, color='black', alpha=0.2, ax=ax1)
		self.df['close'].plot(linewidth=1, color='black', alpha=0.4, ax=ax1)

		# self.df['Kijun_sen'].plot(linewidth=2, color='blue', alpha=0.4, ax=ax1)

		# self.df['line_1'].plot(linewidth=1)
		# self.df['line_2'].plot(linewidth=1)
		# self.df['line_3'].plot(linewidth=1)
		# self.df['line_4'].plot(linewidth=1)
		# self.df['line_5'].plot(linewidth=1)

		# self.df['shortSignal10'].plot(linewidth=5, color='red', alpha=0.8)
		# self.df['longSignal10'].plot(linewidth=5, color='b', alpha=0.8)
		# self.df['shortSignal5'].plot(linewidth=3, color='red', alpha=0.8, ax=ax1)
		# self.df['longSignal5'].plot(linewidth=3, color='b', alpha=0.8, ax=ax1)
		self.df['shortSignal2'].plot(linewidth=2, color='red', alpha=0.8, ax=ax1)
		self.df['longSignal2'].plot(linewidth=2, color='b', alpha=0.8, ax=ax1)
		# self.df['sma1'     ].plot(linewidth=2, color='red')
		# self.df['shortSignal1'].plot(linewidth=1, color='red', alpha=1)
		# self.df['longSignal1'].plot(linewidth=1, color='b', alpha=1)
		# self.df.plot(kind='scatter', x='Unnamed: 0', y='close', linewidth=1)
		# self.df['hammer'].replace('0',np.nan, inplace=True)
		# self.df['hammer'].replace('100',self.df.close, inplace=True)
		# plt.scatter(self.df.index, self.df['close'], c='green', alpha=0.9, s=5)

		# print(self.df.hammer)
		# self.df['Hammer'].plot(linewidth=2, color='b', alpha=0.8, ax=ax1)
		ax1.scatter(self.df.index, self.df['Hammer'], c='green', alpha=0.9)
		# for index, row in self.df.iterrows():
		# 	if row.Hammer == 100:
		# 		ax1.scatter(row.index, row['close'], c='green', alpha=0.9)

		ax2.fill_between(self.df.index, self.df['macdhist'], 0, color='black', alpha=0.5)

		plt.show()

	#------------------------------------------------------------------
	def endDateFix(self, endDate):
		x =  np.where(self.df.Date == endDate)[0][0]
		# print('index is ',str(x))
		x += 1
		try:
			return self.df.iloc[x].close
		except Exception as e:
			return np.nan	

#-----------------------------------------------------------------------------------#
# helper functions

def is_flat(value, diff):
	if diff == 0:
		return value

def is_peak(values):
	if values[1] > values[0] and values[1] > values[2]:
		return values[1]
	else:
		return np.nan

def is_trough(values):
	if values[1] < values[0] and values[1] < values[2]:
		return values[1]
	else:
		return np.nan

def longSignal(close, line_1, line_2=-100, line_3=-100, line_4=-100, line_5=-100, line_6=-100, line_7=-100, line_8=-100, line_9=-100, line_10=-100):
	
	maxValue = max(line_1, line_2, line_3, line_4, line_5, line_6, line_7, line_8, line_9, line_10) 
	if   close > maxValue:
		return maxValue
	else:
		return np.nan

def shortSignal(close, line_1, line_2=100, line_3=100, line_4=100, line_5=100, line_6=100, line_7=100, line_8=100, line_9=100, line_10=100):
	minValue = min(line_1, line_2, line_3, line_4, line_5, line_6, line_7, line_8, line_9, line_10) 
	if close < minValue:
		return minValue
	else:
		return np.nan

def cloudU(line_1, line_2=-100, line_3=-100, line_4=-100, line_5=-100, line_6=-100, line_7=-100, line_8=-100, line_9=-100, line_10=-100):
	maxValue = max(line_1, line_2, line_3, line_4, line_5, line_6, line_7, line_8, line_9, line_10) 
	return maxValue

def cloudD(line_1, line_2=100, line_3=100, line_4=100, line_5=100, line_6=100, line_7=100, line_8=100, line_9=100, line_10=100):
	minValue = min(line_1, line_2, line_3, line_4, line_5, line_6, line_7, line_8, line_9, line_10) 
	return minValue

def enterTrade(close, longSignal, shortSignal):
	if close > longSignal and longSignal > 0:
		return 1
	elif close < shortSignal and shortSignal > 0:
		return -1
	else:
		return 0

def minFinder(close, max_down, signal, openPrice):
	if signal == 1:
		if (openPrice - close) > max_down:
			return (openPrice - close)
	return max_down

def maxFinder(close, max_up, signal, openPrice):
	if signal == -1:
		if (close - openPrice) > max_up:
			return (close - openPrice)
	return max_up

def maxProfit(max_up, max_down, signal):
	if signal == 1:
		return max_up
	else:
		return max_down

def maxLoss(max_up, max_down, signal):
	if signal == -1:
		return max_up
	else:
		return max_down

def create_labels(current, future):
    if future > current:
        return 1
    else:
        return -1

#---------------------------

def learning(): # df global
	global df
	df  = df[['SMA','WMA','Label']]  # df.drop(['Label', 'ISM'],1)
	df.dropna(inplace= True)

	Xdf = df[['SMA','WMA']]  # df.drop(['Label', 'ISM'],1)
	# Xdf.replace(np.nan, 999, inplace=True)
	print(Xdf.head())
	X = np.array(Xdf)
	X = preprocessing.scale(X)
	y = np.array(df['Label'])

	results = 0
	for a in range(1,2):
	    X_train, X_test, y_train, y_test = model_selection.train_test_split(X,y,test_size=0.2)
	    clf = svm.SVC()
	    clf.fit(X_train, y_train)
	    results += clf.score(X_test, y_test)

	return 'accuracy = ', results#/2

#---------------------------
























