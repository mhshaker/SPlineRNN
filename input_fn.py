
import tensorflow as tf
from tensorflow.contrib import rnn
import numpy as np
import pandas as pd  
import matplotlib.pyplot as plt
import os
import forexLSTMData as price
import tf_util as util
import random
random.seed(1)
tf.set_random_seed(1)
np.random.seed(1)

def read_dataset(x,y, batch_size, mode=tf.estimator.ModeKeys.TRAIN):
    X = tf.constant(x, name='input_X', dtype=tf.float32)
    labels = tf.constant(y, name='labels')
    features = {"input_X": X} 

    dataset = tf.data.Dataset.from_tensor_slices((dict(features), labels))
    if mode == tf.estimator.ModeKeys.EVAL:
    	dataset = dataset.repeat(1).batch(batch_size)
    else:
    	dataset = dataset.repeat().batch(batch_size)
    return dataset.make_one_shot_iterator().get_next()

def predict_input_fn(x, batch_size=1):
    """An input function for evaluation or prediction"""
    X = tf.constant(x, name='input_X', dtype=tf.float32)
    features = {"input_X": X} 
    inputs = dict(features)
    
    # Convert the inputs to a Dataset.
    dataset = tf.data.Dataset.from_tensor_slices(inputs)

    # Batch the examples
    assert batch_size is not None, "batch_size must not be None"
    dataset = dataset.batch(batch_size)
    return dataset

